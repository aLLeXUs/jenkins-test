FROM node:10-alpine

WORKDIR /app/

COPY ./backend/node_modules /app/node_modules
COPY ./backend/package*.json /app/
COPY ./backend/dist /app/
COPY ./backend/.env /app/.env
COPY ./backend/.env.test /app/
COPY ./backend/public /app/public/
COPY ./backend/jest-ci.config.json /app/
COPY ./client/build /app/public/

CMD ["npm", "run", "start-forever"]
